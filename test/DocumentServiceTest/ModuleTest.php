<?php
namespace DocumentServiceTest;

use DocumentService\Module;
use PHPUnit\Framework\TestCase;
use Zend\ModuleManager\Feature\ConfigProviderInterface;

/**
 * Test for the Module class
 * @author heik
 * @covers DocumentService\Module
 */
class ModuleTest extends TestCase
{
    /**
     * Test interfaces
     */
    public function testInterface()
    {
        $module = new Module();
        $this->assertInstanceOf(ConfigProviderInterface::class, $module);
        $this->assertInternalType('array', $module->getConfig());
    }
}
