<?php
namespace DocumentService\EventListener;

use DocumentService\EventManager\Event;
use Psr\Log\LoggerInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\ListenerAggregateInterface;

/**
 * Log document activities (add, get, update, replace, delete)
 * @author heik
 */
class LogListener implements ListenerAggregateInterface
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger = null)
    {
        $this->logger = $logger;
    }

    /**
     * @param EventManagerInterface $events
     */
    public function detach(EventManagerInterface $events)
    {
        $events->detach([$this, 'onDocumentAdd']);
        $events->detach([$this, 'onDocumentGet']);
        $events->detach([$this, 'onDocumentUpdate']);
        $events->detach([$this, 'onDocumentReplace']);
        $events->detach([$this, 'onDocumentDelete']);
    }

    /**
     * @param EventManagerInterface $events
     * @param number $priority
     */
    public function attach(EventManagerInterface $events, $priority = 1)
    {
        $events->attach(Event\DocumentAddEvent::NAME, [$this, 'onDocumentAdd'], $priority);
        $events->attach(Event\DocumentGetEvent::NAME, [$this, 'onDocumentGet'], $priority);
        $events->attach(Event\DocumentUpdateEvent::NAME, [$this, 'onDocumentUpdate'], $priority);
        $events->attach(Event\DocumentReplaceEvent::NAME, [$this, 'onDocumentReplace'], $priority);
        $events->attach(Event\DocumentDeleteEvent::NAME, [$this, 'onDocumentDelete'], $priority);
    }

    /**
     * @param Event\DocumentAddEvent $event
     */
    public function onDocumentAdd(Event\DocumentAddEvent $event)
    {
        $doc = $event->getDocument();
        $this->logInfo(
            sprintf("DocumentAddEvent : Document %d (%s) added.", $doc->getId(), $doc->getTitle())
        );
    }

    /**
     * @param Event\DocumentGetEvent $event
     */
    public function onDocumentGet(Event\DocumentGetEvent $event)
    {
        $doc = $event->getDocument();
        if ($doc) {
            $this->logInfo(
                sprintf("DocumentGetEvent : Document %d (%s) fetched.", $doc->getId(), $doc->getTitle())
            );
        } else {
            $this->logError(
                sprintf("DocumentGetEvent : Document %d could not be found.", $event->getId())
            );
        }
    }

    /**
     * @param Event\DocumentUpdateEvent $event
     */
    public function onDocumentUpdate(Event\DocumentUpdateEvent $event)
    {
        $doc = $event->getDocument();
        $this->logInfo(
            sprintf("DocumentUpdateEvent : Document %d (%s) was updated.", $doc->getId(), $doc->getTitle())
        );
    }

    /**
     * @param Event\DocumentReplaceEvent $event
     */
    public function onDocumentReplace(Event\DocumentReplaceEvent $event)
    {
        $doc = $event->getDocument();
        $this->logInfo(
            sprintf("DocumentReplaceEvent : Document %d (%s) was replaced.", $doc->getId(), $doc->getTitle())
        );
    }

    /**
     * @param Event\DocumentDeleteEvent $event
     */
    public function onDocumentDelete(Event\DocumentDeleteEvent $event)
    {
        $doc = $event->getDocument();
        $this->logInfo(
            sprintf("DocumentDeleteEvent : Document %d (%s) was deleted.", $doc->getId(), $doc->getTitle())
        );
    }

    /**
     * @param string $message
     */
    private function logInfo($message)
    {
        if (!$this->logger) {
            return;
        }

        $this->logger->info($message);
    }

    /**
     * @param string $message
     */
    private function logError($message)
    {
        if (!$this->logger) {
            return;
        }

        $this->logger->error($message);
    }
}
