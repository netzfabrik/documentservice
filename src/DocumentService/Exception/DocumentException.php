<?php
namespace DocumentService\Exception;

/**
 * Exception in DocumentService
 * @author heik
 */
class DocumentException extends \Exception
{
}
