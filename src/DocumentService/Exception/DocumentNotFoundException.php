<?php
namespace DocumentService\Exception;

/**
 * Document could not be found
 * @author heik
 */
class DocumentNotFoundException extends DocumentException
{
}
