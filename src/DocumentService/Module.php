<?php
namespace DocumentService;

use Zend\ModuleManager\Feature\ConfigProviderInterface;

/**
 * Module class
 * @author heik
 */
class Module implements ConfigProviderInterface
{
    /**
     * {@inheritDoc}
     * @see \Zend\ModuleManager\Feature\ConfigProviderInterface::getConfig()
     */
    public function getConfig()
    {
        return include __DIR__ . '/../../config/module.config.php';
    }
}
