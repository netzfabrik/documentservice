<?php
namespace DocumentService\Repository\Factory;

use Doctrine\ORM\Mapping\ClassMetadata;
use DocumentService\Entity\Document;
use DocumentService\Repository\DocumentRepository;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Factory for the DocumentRepository
 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
 * @author heik
 */
class DocumentRepositoryFactory implements FactoryInterface
{
    /**
     * (non-PHPdoc)
     * @see \Zend\ServiceManager\Factory\FactoryInterface::__invoke()
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $classMeta = new ClassMetadata(Document::class);
        return new DocumentRepository($entityManager, $classMeta);
    }
}
