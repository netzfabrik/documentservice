<?php
namespace DocumentService\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Repository for Document entities
 * @author heik
 */
class DocumentRepository extends EntityRepository
{
    /**
     * Persist entity
     * @param object $entity
     */
    public function persist($entity)
    {
        $this->getEntityManager()->persist($entity);
    }

    /**
     * Remove entity
     * @param object $entity
     */
    public function remove($entity)
    {
        $this->getEntityManager()->remove($entity);
    }

    /**
     * Flush operation
     * @param object $entity
     */
    public function flush($entity = null)
    {
        $this->getEntityManager()->flush($entity);
    }
}
