<?php
namespace DocumentService\EventManager\Event;

/**
 * Document was updated
 * @author heik
 */
class DocumentUpdateEvent extends AbstractEvent
{
    const NAME = 'document-update';
}
