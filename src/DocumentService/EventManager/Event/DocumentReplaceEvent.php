<?php
namespace DocumentService\EventManager\Event;

/**
 * Document was replaced
 * @author heik
 */
class DocumentReplaceEvent extends AbstractEvent
{
    const NAME = 'document-replace';
}
