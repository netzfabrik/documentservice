<?php
namespace DocumentService\EventManager\Event;

use DocumentService\Entity\Document;
use Zend\EventManager\Event;

/**
 * Abstract event class providing base functionality for document events
 */
abstract class AbstractEvent extends Event
{
    const NAME = '';

    /**
     * @var Document
     */
    private $document;

    /**
     * @param Document $document
     */
    public function __construct(Document $document = null)
    {
        $this->document = $document;
        parent::__construct(static::NAME);
    }

    /**
     * @return Document
     */
    public function getDocument()
    {
        return $this->document;
    }
}
