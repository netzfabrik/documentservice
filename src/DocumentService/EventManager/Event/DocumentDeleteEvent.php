<?php
namespace DocumentService\EventManager\Event;

/**
 * Document was deleted
 * @author heik
 */
class DocumentDeleteEvent extends AbstractEvent
{
    const NAME = 'document-delete';
}
