<?php
namespace DocumentService\EventManager\Event;

/**
 * Document was fetched
 * @author heik
 */
class DocumentGetEvent extends AbstractEvent
{
    const NAME = 'document-get';
}
