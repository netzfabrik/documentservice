<?php
namespace DocumentService\EventManager\Event;

/**
 * Document was added
 * @author heik
 */
class DocumentAddEvent extends AbstractEvent
{
    const NAME = 'document-add';
}
