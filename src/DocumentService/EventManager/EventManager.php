<?php
namespace DocumentService\EventManager;

use Zend\EventManager\EventManager as ZendEventManager;

/**
 * EventManager for DocumentService
 * @author heik
 */
class EventManager extends ZendEventManager
{
    /**
     * Create EventManager for DocumentService
     */
    public function __construct()
    {
        parent::__construct(null, ['document-service']);
    }
}
