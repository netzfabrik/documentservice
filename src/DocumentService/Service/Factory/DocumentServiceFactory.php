<?php
namespace DocumentService\Service\Factory;

use DocumentService\EventListener\LogListener;
use DocumentService\EventManager\EventManager;
use DocumentService\Repository\DocumentRepository;
use DocumentService\Service\DocumentService;
use DocumentService\Service\DocumentServiceConfiguration;
use Interop\Container\ContainerInterface;
use Psr\Log\AbstractLogger;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Factory for the DocumentService
 * @author heik
 */
class DocumentServiceFactory implements FactoryInterface
{
    /**
     * {@inheritDoc}
     * @see \Zend\ServiceManager\Factory\FactoryInterface::__invoke()
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $config = $container->get('Config');
        $configurationArray = $config['service_documents'];

        $configuration = new DocumentServiceConfiguration($configurationArray);
        $repository = $container->get(DocumentRepository::class);

        /* @var $eventManager EventManager */
        $eventManager = $container->get(EventManager::class);

        // attach log listener - if logger is given
        if ($logger = $configurationArray['logger']) {

            if (is_string($logger)) {
                // find in servicemanager
                $logger = $container->get($logger);
            }

            if ($logger instanceof AbstractLogger) {
                $loggerListener = new LogListener($logger);
                $loggerListener->attach($eventManager);
            }
        }

        return new DocumentService($configuration, $repository, $eventManager);
    }
}
