<?php
namespace DocumentService\Service;

/**
 * Configuration object for DocumentService
 * @author heik
 */
class DocumentServiceConfiguration
{
    /**
     * @var string
     */
    private $storagePath;

    /**
     * @var int
     */
    private $folderDepth = 2;

    /**
     * @var int
     */
    private $folderLength = 3;

    /**
     * @var int
     */
    private $filenameLength = 10;

    /**
     * Create with configuration array
     * @param array $configuration
     */
    public function __construct(array $configuration)
    {
        if (isset($configuration['storage_path'])) {
            $this->storagePath = $configuration['storage_path'];
        }
        if (isset($configuration['folder_depth'])) {
            $this->folderDepth = $configuration['folder_depth'];
        }
        if (isset($configuration['folder_length'])) {
            $this->folderLength = $configuration['folder_length'];
        }
        if (isset($configuration['filename_length'])) {
            $this->filenameLength = $configuration['filename_length'];
        }
    }

    /**
     * @return string
     */
    public function getStoragePath()
    {
        return $this->storagePath;
    }

    /**
     * @return int
     */
    public function getFolderDepth()
    {
        return $this->folderDepth;
    }

    /**
     * @return int
     */
    public function getFolderLength()
    {
        return $this->folderLength;
    }

    /**
     * @return int
     */
    public function getFilenameLength()
    {
        return $this->filenameLength;
    }
}
