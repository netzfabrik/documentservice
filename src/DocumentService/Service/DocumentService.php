<?php
namespace DocumentService\Service;

use DocumentService\Entity\Document;
use DocumentService\EventManager\Event;
use DocumentService\EventManager\EventManager;
use DocumentService\Exception\DocumentNotFoundException;
use DocumentService\Repository\DocumentRepository;
use DocumentService\Exception\DocumentException;

/**
 * Document service
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @author heik
 */
class DocumentService implements DocumentServiceInterface
{
    /**
     * @var DocumentServiceConfiguration
     */
    private $configuration;

    /**
     * @var DocumentRepository
     */
    private $repository;

    /**
     * @var EventManager
     */
    private $eventManager;

    /**
     * @param DocumentServiceConfiguration $configuration
     * @param DocumentRepository $repository
     * @param EventManager $eventManager
     */
    public function __construct(
        DocumentServiceConfiguration $configuration,
        DocumentRepository $repository,
        EventManager $eventManager
    ) {
        $this->repository = $repository;
        $this->configuration = $configuration;
        $this->eventManager = $eventManager;
    }

    /**
     * {@inheritDoc}
     * @see \DocumentService\Service\DocumentServiceInterface::addDocument()
     */
    public function addDocument(array $uploadData, array $postData = [])
    {
        $document = new Document();
        $document->setCreated(new \DateTime());

        $this->processUploadedFile($document, $uploadData, $postData);

        $this->getRepository()->persist($document);
        $this->getRepository()->flush($document);

        $event = new Event\DocumentAddEvent($document);
        $this->eventManager->triggerEvent($event);

        return $document;
    }

    /**
     * {@inheritdoc}
     * @see \DocumentService\Service\DocumentServiceInterface::getDocument()
     */
    public function getDocument($id)
    {
        $doc = $this->getRepository()->find($id);
        if (!$doc) {
            throw new DocumentNotFoundException();
        }

        $event = new Event\DocumentGetEvent($doc, $id);
        $this->eventManager->triggerEvent($event);

        return $doc;
    }

    /**
     * {@inheritDoc}
     * @see \DocumentService\Service\DocumentServiceInterface::updateDocument()
     */
    public function updateDocument($id, array $data = [])
    {
        $document = $this->getDocument($id);

        // @todo - implement

        $event = new Event\DocumentUpdateEvent($document);
        $this->eventManager->triggerEvent($event);

        return $document;
    }

    /**
     * {@inheritDoc}
     * @see \DocumentService\Service\DocumentServiceInterface::replaceDocument()
     */
    public function replaceDocument($id, array $uploadData, array $postData = [])
    {
        $document = $this->getRepository()->find($id);
        if (!$document) {
            $document = new Document();
            $document->setCreated(new \DateTime());
        }
        $this->processUploadedFile($document, $uploadData, $postData);

        $this->getRepository()->persist($document);
        $this->getRepository()->flush($document);

        $event = new Event\DocumentReplaceEvent($document);
        $this->eventManager->triggerEvent($event);

        return $document;
    }

    /**
     * {@inheritDoc}
     * @see \DocumentService\Service\DocumentServiceInterface::deleteDocument()
     */
    public function deleteDocument($id)
    {
        $doc = $this->getDocument($id);

        $event = new Event\DocumentDeleteEvent($doc);
        $this->eventManager->triggerEvent($event);

        if (!$doc) {
            throw new DocumentNotFoundException();
        }

        $this->getRepository()->remove($doc);
        $this->getRepository()->flush($doc);
    }

    /**
     * Get files contents
     * @param Document $document
     * @return string
     */
    public function getContent(Document $document)
    {
        $path = sprintf("%s/%s", $this->configuration->getStoragePath(), $document->getPath());
        return file_get_contents($path);
    }

    /**
      * @param Document $document
      * @param array $uploadData
      * @param array $postData
      */
    private function processUploadedFile(Document $document, $uploadData, $postData)
    {
        if (empty($uploadData['tmp_name'])) {
            throw new DocumentException('Uploaded file could not be read.');
        }

        $fileName = $uploadData['tmp_name'];

        $prefix = null;
        if (!empty($postData['prefix'])) {
            $prefix = $postData['prefix'];
        }

        if (isset($postData['type'])) {
            $document->setType($postData['type']);
        }

        if (!empty($postData['title'])) {
            $title = $postData['title'];
        } else {
            if (!empty($uploadData['name'])) {
                $title = substr($uploadData['name'], 0, strrpos($uploadData['name'], '.'));
            } else {
                $randomName = md5(filemtime($fileName) . microtime(true));
                $title = substr($randomName, 0, $this->configuration->getFilenameLength());
            }
        }

        $document->setTitle($title);
        $document->setMime(mime_content_type($fileName));
        if (isset($uploadData['name'])) {
            $document->setExtension(substr($uploadData['name'], strrpos($uploadData['name'], '.') + 1));
        }

        if ($prefix) {
            // prefixed path in storage
            $pathInStorage = $prefix;
        } else {
            // create upload folder
            $depth = $this->configuration->getFolderDepth();
            $randomName = md5(microtime(true) . $fileName);
            $folders = str_split($randomName, $this->configuration->getFolderLength());
            $pathInStorage = 'common/' . implode('/', array_slice($folders, 0, $depth));
        }

        $targetFileName = sprintf("%s.%s", $title, $document->getExtension());
        $destination = sprintf("%s/%s/%s", $this->configuration->getStoragePath(), $pathInStorage, $targetFileName);

        // create storage path if needed
        $destinationFolder = pathinfo($destination, PATHINFO_DIRNAME);
        if (!is_dir($destinationFolder)) {
            mkdir($destinationFolder, 0755, true);
        }

        if (!move_uploaded_file($fileName, $destination)) {
            throw new DocumentException('Moving uploaded file failed.');
        }

        $document->setPath(sprintf("%s/%s", $pathInStorage, $targetFileName));
    }

    /**
     * @return DocumentRepository
     */
    private function getRepository()
    {
        return $this->repository;
    }
}
