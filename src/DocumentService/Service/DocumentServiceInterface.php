<?php
namespace DocumentService\Service;

use DocumentService\Entity\Document;

/**
 * @author heik
 */
interface DocumentServiceInterface
{
    /**
     * Add new document
     * @param array $uploadData
     * @param array $postData
     * @return Document
     */
    public function addDocument(array $uploadData, array $postData = []);

    /**
     * Fetch a document by id
     * @param int $id
     * @return Document
     */
    public function getDocument($id);

    /**
     * Update existing document
     * @param int $id
     * @param array $postData
     * @return Document
     */
    public function updateDocument($id, array $postData = []);

    /**
     * Replace existing document
     * @param int $id
     * @param array $uploadData
     * @param array $postData
     * @return Document
     */
    public function replaceDocument($id, array $uploadData, array $postData = []);

    /**
     * Delete document
     * @param int $id
     */
    public function deleteDocument($id);
}
