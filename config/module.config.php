<?php
namespace DocumentService;

use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use DocumentService\EventManager\EventManager;
use Psr\Log\NullLogger;
use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'service_manager' => [
        'factories' => [
            Service\DocumentService::class => Service\Factory\DocumentServiceFactory::class,
            Repository\DocumentRepository::class => Repository\Factory\DocumentRepositoryFactory::class,
            EventManager::class => InvokableFactory::class,
            NullLogger::class => InvokableFactory::class,
        ]
    ],

    'service_documents' => [
        'storage_path' => sys_get_temp_dir() . '/documentService',
        'folder_depth' => 2,
        'folder_length' => 4,
        'filename_length' => 10,
        // logger class
        'logger' => NullLogger::class
    ],

    'doctrine' => [
        'driver' => [
            'documentservice_driver' => [
                'class' => AnnotationDriver::class,
                'cache' => 'array',
                'paths' => [
                    __DIR__ . '/../src/DocumentService/Entity',
                ],
            ],
            'orm_default' => [
                'drivers' => [
                    'DocumentService\\Entity' => 'documentservice_driver'
                ],
            ],
        ],
    ]
];
